﻿using UnityEngine;
using SWNetwork;
using System.Net.Configuration;

public class BallController : MonoBehaviour
{
    [SerializeField]
    private float PlayerBouncePower;
    [SerializeField]
    private float WallBouncePower;
    [SerializeField]
    private Vector3 ResetBallPosition;
    [SerializeField]
    private GameController gameController;

    private Rigidbody _ballRB;
    private Vector3 lastVel;
    private NetworkID networkID;
    private RemoteEventAgent remoteEvent;

    private void Start()
    {
        _ballRB = GetComponent<Rigidbody>();
        networkID = GetComponent<NetworkID>();
        remoteEvent = GetComponent<RemoteEventAgent>();
    }

    private void Update()
    {
        lastVel = _ballRB.velocity;

        if (transform.position.y <= -1f)
        {
            transform.position = new Vector3(transform.position.x, 2f, transform.position.z);
        }

        if (Input.GetKeyDown(KeyCode.RightBracket))
        {
            Invoke("ResetBall", 0f);
            BallScore(2);
        }
        if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            Invoke("ResetBall", 0f);
            BallScore(1);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Goal1")
        {
            Invoke("ResetBall", 0f);
            BallScore(2);
        }
        else if (other.gameObject.tag == "Goal2")
        {
            Invoke("ResetBall", 0f);
            BallScore(1);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        var speed = lastVel.magnitude;
        var dir = Vector3.Reflect(lastVel.normalized, collision.GetContact(0).normal);

        if (collision.gameObject.tag == "Player")
        {
            //_ballRB.velocity = dir * Mathf.Max(speed, 0f) * PlayerBouncePower;
        }
        else
        {
            _ballRB.velocity = dir * Mathf.Max(speed, 0f) * WallBouncePower;
        }
    }

    public void ForceBall(Vector3 normal) {

        print("this is called");

        var speed = lastVel.magnitude;
        var dir = Vector3.Reflect(lastVel.normalized, normal);
        
        _ballRB.velocity = dir * Mathf.Max(speed, 0f) * PlayerBouncePower;

    }

    void ResetBall() {
        SWNetworkMessage msg = new SWNetworkMessage();
        msg.Push(ResetBallPosition);
        remoteEvent.Invoke("ResetBall", msg);
    }

    public void RemoteResetBall(SWNetworkMessage msg) {
        Vector3 pos = msg.PopVector3();
        this.transform.position = pos;
    }

    void BallScore(int teams) {
        GameController gameController = FindObjectOfType<GameController>();
        gameController.AddTeamScore(teams);
    }

}
