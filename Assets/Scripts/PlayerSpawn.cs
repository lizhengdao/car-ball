﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{

    [SerializeField] private GameObject playerPrefab = null;

    private static List<Transform> spawnPoints = new List<Transform>();
    private int nextIndex = 0;

    public static void AddSpawnPoint(Transform pos){
        spawnPoints.Add(pos);
        spawnPoints = spawnPoints.OrderBy(x => x.GetSiblingIndex()).ToList();
    }

    public static void RemoveSpawnPoint(Transform pos) => spawnPoints.Remove(pos);

    //[Server]
    //public void SpawnPlayer(NetworkConnection conn) {

    //    Transform spawnPoint = spawnPoints.ElementAtOrDefault(nextIndex);

    //    if (spawnPoint == null)
    //    {
    //        Debug.LogError("Missing spawn point for player : " + nextIndex.ToString());
    //        return;
    //    }

    //    GameObject playerInstance = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
    //    NetworkServer.Spawn(playerInstance, conn);

    //    nextIndex++;
    //}

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
