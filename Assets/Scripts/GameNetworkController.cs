﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameNetworkController : MonoBehaviour
{
    [Serializable]
    public class Score
    {
        public int Team1Score;
        public int Team2Score;
    }
}
